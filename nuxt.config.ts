import { defineNuxtConfig } from "nuxt";
import { resolve, dirname } from "node:path";
import { fileURLToPath } from "url";
import VueI18nVitePlugin from "@intlify/unplugin-vue-i18n/vite";

export default defineNuxtConfig({
  app: {
    head: {
      title: "IP | HahuJobs",
      htmlAttrs: {
        lang: "en",
      },
      meta: [
        { "data-n-head": "ssr", charset: "utf-8" },
        { "x-robots-tag": "all" },
        {
          "data-n-head": "ssr",
          name: "viewport",
          content: "width=device-width, initial-scale=1",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "description",
          name: "description",
          content:
            "The platform uses biometric based identification to both register workers and job seekers. The process begins with mass registration of employment seekers to build a digital pool of job seekers.",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:url",
          property: "og:url",
          content: "https://industryparks.hahu.jobs/",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:type",
          property: "og:type",
          content: "website",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:title",
          property: "og:title",
          content: "IP | HahuJobs",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:description",
          property: "og:description",
          content:
            "The platform uses biometric based identification to both register workers and job seekers. The process begins with mass registration of employment seekers to build a digital pool of job seekers.",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "og:image",
          property: "og:image",
          content: "https://industryparks.hahu.jobs/images/Hahu-ip-meta.png",
        },

        // twitter
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:card",
          property: "twitter:card",
          content: "summary_large_image",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:domain",
          property: "twitter:domain",
          content: "industryparks.hahu.jobs",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:url",
          property: "twitter:url",
          content: "https://industryparks.hahu.jobs/",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:title",
          property: "twitter:title",
          content: "IP | HahuJobs",
        },
        {
          "data-n-head": "ssr",
          "data-hid": "twitter:description",
          property: "twitter:description",
          content:
            "The platform uses biometric based identification to both register workers and job seekers. The process begins with mass registration of employment seekers to build a digital pool of job seekers.",
        },
        {
          "data-hid": "twitter:image",
          content: "https://industryparks.hahu.jobs/images/Hahu-ip-meta.png",
        },
        {
          "data-hid": "twitter:image",
          "data-n-head": "ssr",
          property: "twitter:image",
          content: "https://industryparks.hahu.jobs/images/Hahu-ip-meta.png",
        },
      ],
      link: [
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/x-icon",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "16x16",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "32x32",
          href: "/images/favicon.ico",
        },
        {
          "data-n-head": "ssr",
          rel: "icon",
          type: "image/png",
          sizes: "96x96",
          href: "/images/favicon.ico",
        },
        { rel: "preconnect", href: "https://fonts.gstatic.com" },
        { href: "https://www.google-analytics.com", rel: "preconnect" },

        {
          href: "https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;600;700;800;900&display=swap",
          rel: "stylesheet",
        },
        { rel: "preconnect", href: "https://fonts.googleapis.com" },
        // {
        //   href: "https://fonts.googleapis.com/css2?family=Poppins:wght@500;700&display=swap",
        //   rel: "stylesheet",
        // },
      ],
      script: [],
    },
  },
  css: ["@/assets/css/styles.css"],
  publicRuntimeConfig: {
    secret: process.env.MEASUREMENT_ID,
  },
  modules: ["@nuxtjs/color-mode"],
  build: {
    transpile: ["@headlessui/vue", "@heroicons/vue"],
    postcss: {
      postcssOptions: require("./postcss.config.js"),
    },
  },
  vite: {
    plugins: [
      VueI18nVitePlugin({
        include: [
          resolve(dirname(fileURLToPath(import.meta.url)), "./locales/*.json"),
        ],
      }),
    ],
  },

  colorMode: {
    classPrefix: "",
    classSuffix: "",
  },
});
