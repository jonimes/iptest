import { createI18n } from 'vue-i18n';
import am from '../locales/am.json';
import en from '../locales/en.json';
export default defineNuxtPlugin(({ vueApp }) => {
    const i18n = createI18n({
        legacy: false,
        globalInjection: true,
        locale: "en",
        messages: {
            en,
            am
        }
    });
    vueApp.use(i18n);
});
